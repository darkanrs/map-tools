'use strict';

import Players from "../model/Players.js";
import {Position} from "../model/Position.js";

export var PlayerControl = L.Control.extend({
  options: {
    position: 'topleft'
  },

  onAdd: function(map) {
    var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control noselect');
    container.style.background = 'none';
    container.style.width = '100px';
    container.style.height = 'auto';

    var labelsButton = L.DomUtil.create('a', 'leaflet-bar leaflet-control leaflet-control-custom', container);
    labelsButton.id = 'toggle-map-players';
    labelsButton.innerHTML = 'Toggle Players';

    L.DomEvent.on(labelsButton, 'click', this._togglePlayers, this);

    this._enabled = true;
    this._markers = {};

    var self = this;
    this._interval = setInterval(() => {
      $.getJSON('/api/world/players', function(data) {
        for (let i = 0;i < data.length;i++) {
          let x = data[i].coords.x;
          let y = data[i].coords.y;
          if (!self._markers[data[i].name.replace(/ /g, '_')]) {
            self._markers[data[i].name.replace(/ /g, '_')] = L.marker(new Position(x, y, 0).toLatLng(map), { title: data[i].name });
            self._markers[data[i].name.replace(/ /g, '_')].addTo(map);
          } else {
            self._markers[data[i].name.replace(/ /g, '_')].setLatLng(new Position(x, y, 0).toLatLng(map));
          }
        }
      });
    }, 1000);

    L.DomEvent.disableClickPropagation(container);
    return container;
  },

  _togglePlayers: function() {
    if (this._enabled) {
      this._map.getPane("map-players").style.display = "none";
      this._enabled = false;
    } else {
      this._map.getPane("map-players").style.display = "";
      this._enabled = true;
    }
  }
});
